# DotNetARM

This is an ARM64 only version of the PKGBUILD for installing DotNet Preview.Original work is based off the [aur repo](https://aur.archlinux.org/packages/dotnet-host-preview/) that was having some issues deploying the SDK. 

Due to frustration and lack of patience, the deployment process was simplified. This PKGBUILD will be used for a shell runner for dotnet apps located else where in this subproject in GitLab.

## Use

To use:
1. Ensure you are on an ARM device using the ARM64 bit kernel (`uname -a` and look for `aarch64`)
1. Either checkout the repo or just copy the PKGBuild to a folder (recommended `~/aur/dotnet-preview`)
1. `makepkg -si` to build and install
1. Do dotnet stuff! 


## Modification

If you change the hash, source, and arch lines this can be built easily for AMD64 or ARMv7 (Raspberry Pi 2 or older kernel pi's effectively). 

